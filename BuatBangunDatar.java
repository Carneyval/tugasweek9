package week9;

public class BuatBangunDatar{
    public static void main(String[] args) {
        //BANGUN DATAR
        BangunDatar Persegi = new BangunDatar(4);
        BangunDatar PP = new BangunDatar(4, 9);
        BangunDatar Segitiga = new BangunDatar(4, 9);
        BangunDatar Lingkaran = new BangunDatar(5); //LUAS LINGKARAN
        BangunDatar Ling = new BangunDatar(8); //KELILING LINGKARAN
        BangunDatar PersegiK= new BangunDatar(10); //KELILING PERSEGI
        // BangunDatar Trapesium = new BangunDatar(120, 120, 250, 102); 

        //BANGUN RUANG
        BangunRuang balok = new BangunRuang(5, 7, 8);
        BangunRuang kubus = new BangunRuang(7);
        BangunRuang bola = new BangunRuang(7.0);

        System.out.println("Hasil Luas Persegi = "+Persegi.luas(4));
        System.out.println("Hasil Luas Persegi Panjang= "+PP.luas(4, 9));
        System.out.println("Hasil Luas Segitiga = "+Segitiga.luas(4.8, 9.3));
        System.out.println("Hasil Luas Lingkaran = "+Lingkaran.luas(5.0));
        System.out.println("Hasil Keliling Lingkaran = "+Ling.keliling(8.7));
        System.out.println("Hasil Keliling Persegi= "+PersegiK.keliling(10));
        System.out.println("Hasil Volume Balok = "+balok.volume(5, 7, 8));
        System.out.println("Hasil Volume Kubus = "+kubus.volume(7));
        System.out.println("Hasil Volume Bola = "+bola.volume(7.0));
        // System.out.println("Hasil = "+Trapesium.keliling(120.0, 120.41, 250, 102.9));
    }
}