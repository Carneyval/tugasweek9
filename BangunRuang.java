package week9;

public class BangunRuang extends BangunDatar{
    private int tinggi;
    private double r;

    public BangunRuang(int panjang, int lebar, int tinggi){
        super(panjang, lebar);
        this.tinggi = tinggi;
    }

    public BangunRuang(int sisi) {
        super(sisi);
        this.sisi = sisi;
    }

    public BangunRuang(double r) {
        super(0);
        this.r = r;
    }

    public double getR() {
        return r;
    }

    public int getTinggi() {
        return tinggi;
    }
    
    //VOLUME BALOK
    public int volume(int panjang, int lebar, int tinggi) {
        return super.getPanjang() * super.getLebar() * getTinggi();
    }

    //VOLUME KUBUS
    public int volume(int sisi) {
        return super.getSisi() * super.getSisi() * super.getSisi();
    }
    
    //VOLUME BOLA
    public double volume(double r) {
        return (super.getPi() * r * r)*4/3;
    }
}